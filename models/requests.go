package models

// AddProductToCartRequest type for handling add to cart requests
type AddProductToCartRequest struct {
	UserID    string `json:"user_id"`
	ProductID string `json:"product_id"`
	Quantity  int    `json:"quantity"`
}

// RemoveProductFromCartRequest type for handling remove from cart requests
type RemoveProductFromCartRequest struct {
	UserID    string `json:"user_id"`
	ProductID string `json:"product_id"`
	Quantity  int    `json:"quantity"`
}

// GetUserCartRequest type for handling get cart requests
type GetUserCartRequest struct {
	UserID string `json:"user_id"`
}
