package models

// ShoppingCart the type for shopping carts
type ShoppingCart struct {
	UserID   string                 `json:"user_id"`
	Products []ShoppingCartProducts `json:"product_ids"`
}

// ShoppingCartProducts a type for products inside shopping cart
type ShoppingCartProducts struct {
	ProductID string `json:"product_id"`
	Quantity  int    `json:"quantity"`
}
