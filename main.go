package main

import (
	"context"
	"coretrix/config"
	"coretrix/helpers/logger"
	"coretrix/router/httprouter"
	"coretrix/services"
	"fmt"
	"github.com/RediSearch/redisearch-go/redisearch"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	// initializing configs from config.yaml
	err := config.Init()
	if err != nil {
		logger.ZSLogger.Panicf("error initializing config: %s", err)
	}

	// connecting to redis
	redisDriver := &services.Driver{
		DSN:                 []string{fmt.Sprintf("%s:%s", config.Configs.RedisearchHost, config.Configs.RedisearchPort)},
		InternalPoolTimeout: 120,
		IdleTimeout:         600,
		ReadTimeout:         120,
		WriteTimeout:        60,
		PingTry:             true,
	}
	err = redisDriver.Connect()
	if err != nil {
		logger.ZSLogger.Panicf("error connecting to redis: %s", err)
	}

	// connecting to products index in redisearch
	productsClient := redisearch.NewClient(fmt.Sprintf("%s:%s", config.Configs.RedisearchHost, config.Configs.RedisearchPort), "idx:products")

	redisearchService := services.NewRedisearchService(redisDriver, productsClient)
	err = redisearchService.Setup()
	if err != nil {
		logger.ZSLogger.Error("error initializing redisearch")
		panic(err)
	}

	server, router := httprouter.Run(config.Configs.Port)

	httprouter.NewController(router, redisearchService)

	waitForOsSignal()
	log.Println("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}

func waitForOsSignal() {
	osSignal := make(chan os.Signal, 1)
	signal.Notify(osSignal, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	<-osSignal
}
