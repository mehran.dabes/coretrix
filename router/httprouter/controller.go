package httprouter

import (
	"coretrix/models"
	"coretrix/services"
	"github.com/gin-gonic/gin"
	"net/http"
)

type controller struct {
	router            *gin.Engine
	redisearchService services.RedisearchServiceInterface
}

// NewController creates a new controller for handling requests
func NewController(router *gin.Engine, redisearchService services.RedisearchServiceInterface) {
	c := controller{router: router, redisearchService: redisearchService}
	c.register()
}

func (c *controller) register() {
	api := c.router.Group("/api/v1")
	{
		product := api.Group("/product")
		{
			product.POST("/search", c.searchProducts)
			product.GET("/all", c.getAllProducts)
		}
		user := api.Group("/user")
		{
			user.POST("/add-to-cart", c.addProductToCart)
			user.POST("/remove-from-cart", c.removeProductFromCart)
			user.POST("/get-cart", c.getUserCart)
		}
	}
}

func (c *controller) searchProducts(ctx *gin.Context) {
	type request struct {
		Query string `json:"query"`
	}
	var req request
	err := ctx.BindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": "Bad Request"})
		return
	}

	products, err := c.redisearchService.SearchProductsByName(req.Query)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": true, "products": products})
}

func (c *controller) getAllProducts(ctx *gin.Context) {
	products, err := c.redisearchService.GetAllProducts()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": true, "products": products})
}

func (c *controller) addProductToCart(ctx *gin.Context) {
	var req models.AddProductToCartRequest
	err := ctx.BindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": "Bad Request"})
		return
	}

	cart, err := c.redisearchService.AddProductToUserCart(req.UserID, req.ProductID, req.Quantity)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": true, "cart": cart})
}

func (c *controller) removeProductFromCart(ctx *gin.Context) {
	var req models.AddProductToCartRequest
	err := ctx.BindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": "Bad Request"})
		return
	}

	cart, err := c.redisearchService.RemoveProductFromUserCart(req.UserID, req.ProductID, req.Quantity)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": true, "cart": cart})
}

func (c *controller) getUserCart(ctx *gin.Context) {
	var req models.GetUserCartRequest
	err := ctx.BindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": "Bad Request"})
		return
	}

	cart, err := c.redisearchService.GetUserCart(req.UserID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"status": false, "message": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": true, "cart": cart})
}
