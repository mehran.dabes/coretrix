package helpers

import "coretrix/models"

// RemoveElementFromProductsSlice removes an element from the given products slice
func RemoveElementFromProductsSlice(slice []models.ShoppingCartProducts, s int) []models.ShoppingCartProducts {
	return append(slice[:s], slice[s+1:]...)
}
