# Coretrix Trial Project

This is a trial project for coretrix company interview.

In this project the goal is to create a simple shopping cart system for a user. The user can do the following in this service:
- See a list of all products.
- Search through products by name.
- Add products to their cart.
- Remove products from their cart.
- See their shopping cart.

## Table of Content
- [Project Structure](#project-structure)
- [How To Run](#how-to-run)
- [How To Use](#how-to-use)
- [APIs](#apis)
  - [POST /api/v1/product/search](#post-apiv1productsearch)
  - [GET /api/v1/product/all](#get-apiv1productall)
  - [POST /api/v1/user/add-to-cart](#post-apiv1productsearch)
  - [POST /api/v1/user/remove-from-cart](#post-apiv1userremove-from-cart)
  - [POST /api/v1/user/get-cart](#post-apiv1userget-cart)

## Project Structure
```
├───config: contains the configurations for redis 
├───constants: contains the constants such as rediskeys
├───helpers: contains some helper functions
│   └───logger: this is a simple zap logger to use in the project
├───mocks: in this package we create mock data for our project
├───models: contains the types used across the project
├───router
│   └───httprouter: the gin routes are defined in this package
├───services: this contains the main logic of the project
```

The products are stored in an index in `redisearch` so we can search them later on.

The shopping carts are stored inside keys in the format of `carts:#user_id`. Since shopping carts are not usually something we want to keep forever and are temporary,
Redis is used instead of a database.

## How To Run
This project uses `redisearch` for searching through the products. So there is a `docker-compose` file provided to quickly set up a redisearch service on your system.
To run this service you have to run the following command in the root of the project:
```bash
docker-compose -f docker-compose.yaml up -d
```
This command will run the `redisearch` service on port `6379`.

To make sure the command worked correctly you can check if the container is running using `docker ps` command.

After that to run the project you need to run the following command to install the `go` dependencies:
```bash
go mod vendor 
```

And then run this to start the project:
```bash
go run main.go
```

Then the program will be listening on port `8080` for requests.

## How To Use
There 5 APIs in this project to work with.
You can use [this postman link](https://www.getpostman.com/collections/97ee114cf47627ba9ff7) to import the postman collection into your postman to quickly start using the program.

First you have to check the available products and use their ID to add them to a user's shopping cart.
Since there are no authentications in this project, you can enter any user ID you want.
You can see the details of the APIs in the next section.

## APIs
### POST /api/v1/product/search

you can search through products
Body:
```json
{
    "query": "your query"
}
```

Response:
```json
{
  "products": [
	{
	  "id":    "10",
	  "name":  "Product 10",
	  "price": 1000
	},
	{
	  "id":    "9",
	  "name":  "Product 9",
	  "price": 900
	},
	{
	  "id":    "8",
	  "name":  "Product 8",
	  "price": 800
	}
  ]
}
```

### GET /api/v1/product/all

Returns all of the available products

Response:
```json
{
  "products": [
	{
	  "id":    "10",
	  "name":  "Product 10",
	  "price": 1000
	},
	{
	  "id":    "9",
	  "name":  "Product 9",
	  "price": 900
	},
	{
	  "id":    "8",
	  "name":  "Product 8",
	  "price": 800
	}
  ]
}
```

### POST /api/v1/user/add-to-cart

Adds a product to user's shopping cart. If the product already exists, the quantity will be added to the existing value.

Body:
```json
{
    "user_id": "1",
    "product_id": "1",
    "quantity": 2
}
```

Response:
```json
{
    "cart": {
        "user_id": "1",
        "product_ids": [
            {
                "product_id": "1",
                "quantity": 2
            }
        ]
    },
    "status": true
}
```

### POST /api/v1/user/remove-from-cart

Decreases the number of products the user has in their shopping cart.

Body:
```json
{
    "user_id": "1",
    "product_id": "1",
    "quantity": 2
}
```

Response:
```json
{
    "cart": {
        "user_id": "1",
        "product_ids": []
    },
    "status": true
}
```

### POST /api/v1/user/get-cart

Returns the user's shopping cart.

Body:
```json
{
    "user_id": "1"
}
```

Response:
```json
{
    "cart": {
        "user_id": "1",
        "product_ids": [
            {
                "product_id": "1",
                "quantity": 2
            }
        ]
    },
    "status": true
}
```

