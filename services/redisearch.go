package services

import (
	"coretrix/constants"
	"coretrix/helpers"
	"coretrix/helpers/logger"
	"coretrix/mocks"
	"coretrix/models"
	"encoding/json"
	"fmt"
	"github.com/RediSearch/redisearch-go/redisearch"
	"github.com/go-redis/redis"
	"strconv"
)

// RedisearchServiceInterface redisearch service interface
type RedisearchServiceInterface interface {
	Setup() (err error)
	SearchProductsByName(query string) (products []models.Product, err error)
	GetAllProducts() (products []models.Product, err error)
	AddProductToUserCart(userID string, productID string, quantity int) (cart models.ShoppingCart, err error)
	RemoveProductFromUserCart(userID string, productID string, quantity int) (cart models.ShoppingCart, err error)
	GetUserCart(userID string) (cart models.ShoppingCart, err error)
}

// RedisearchService redisearch service struct
type RedisearchService struct {
	redisDriver    *Driver
	productsClient *redisearch.Client
}

// NewRedisearchService creates a new redisearch service
func NewRedisearchService(redisDriver *Driver, productsClient *redisearch.Client) RedisearchServiceInterface {
	return &RedisearchService{
		redisDriver:    redisDriver,
		productsClient: productsClient,
	}
}

// Setup sets up the products schema and connect to redis driver
func (rs *RedisearchService) Setup() (err error) {
	err = rs.createProductsSchema()
	if err != nil {
		return err
	}

	err = rs.redisDriver.Connect()
	return err
}

// SearchProductsByName searches through the products names by the provided query
func (rs *RedisearchService) SearchProductsByName(query string) (products []models.Product, err error) {
	filteredQuery := redisearch.NewQuery(query)
	filteredQuery.InFields = []string{"name"}
	docs, total, err := rs.productsClient.Search(filteredQuery)
	if err != nil {
		logger.ZSLogger.Errorw("error getting products", "err", err)
		return products, constants.ErrInternal
	}
	products = make([]models.Product, total)
	for i, doc := range docs {
		products[i].ID = doc.Id
		products[i].Name = doc.Properties["name"].(string)
		price := doc.Properties["price"].(string)
		priceFloat, err := strconv.ParseFloat(price, 64)
		if err != nil {
			logger.ZSLogger.Errorw("error converting string to float", "err", err)
			return products, constants.ErrInternal
		}
		products[i].Price = priceFloat
	}
	return products, err
}

// SearchProductByID searches products by the provided ID
func (rs *RedisearchService) SearchProductByID(query string) (product models.Product, err error) {
	filteredQuery := redisearch.NewQuery("*")
	filteredQuery.InKeys = []string{query}
	doc, total, err := rs.productsClient.Search(filteredQuery)
	if err != nil {
		logger.ZSLogger.Errorw("error getting product by id", "err", err)
		return product, constants.ErrInternal
	}
	if total == 0 {
		return product, err
	}
	product.ID = doc[0].Id
	product.Name = doc[0].Properties["name"].(string)
	price := doc[0].Properties["price"].(string)
	priceFloat, err := strconv.ParseFloat(price, 64)
	if err != nil {
		logger.ZSLogger.Errorw("error converting string to float", "err", err)
		return product, constants.ErrInternal
	}
	product.Price = priceFloat
	return product, err
}

// GetAllProducts returns all the available products
func (rs *RedisearchService) GetAllProducts() (products []models.Product, err error) {
	docs, total, err := rs.productsClient.Search(redisearch.NewQuery("*"))
	if err != nil {
		logger.ZSLogger.Errorw("error getting all products from redisearch", "err", err)
		return products, constants.ErrInternal
	}
	products = make([]models.Product, total)
	for i, doc := range docs {
		products[i].ID = doc.Id
		products[i].Name = doc.Properties["name"].(string)
		price := doc.Properties["price"].(string)
		priceFloat, err := strconv.ParseFloat(price, 64)
		if err != nil {
			logger.ZSLogger.Errorw("error converting string to float", "err", err)
			return products, constants.ErrInternal
		}
		products[i].Price = priceFloat
	}
	return products, err
}

// AddProductToUserCart Increments the number of products the user have by the provided quantity.
func (rs *RedisearchService) AddProductToUserCart(userID string, productID string, quantity int) (cart models.ShoppingCart, err error) {
	// check if the product exists
	product, err := rs.SearchProductByID(productID)
	if product.ID == "" {
		return cart, constants.ErrNoProductsFound
	}
	// get user's cart from redis
	shoppingCart, err := rs.GetUserCart(userID)
	if err != nil {
		// if the user has no existing cart, we create a new cart
		if err == redis.Nil {
			newShoppingCart := models.ShoppingCart{
				UserID: userID,
				Products: []models.ShoppingCartProducts{
					{ProductID: productID, Quantity: quantity},
				},
			}

			err = rs.saveCartToRedis(newShoppingCart, userID)
			if err != nil {
				logger.ZSLogger.Errorw("error creating new shopping cart for user", "err", err, "user", userID)
				return cart, err
			}
			// we should return nil as the error since the errors value is equal to redis.Nil
			// and we have successfully created a cart for the user
			return newShoppingCart, nil
		}
		logger.ZSLogger.Errorw("error getting user cart from redis", "err", err)
		return cart, constants.ErrInternal
	}

	var hasProductInCart bool
	for i := 0; i < len(shoppingCart.Products); i++ {
		if shoppingCart.Products[i].ProductID == productID {
			shoppingCart.Products[i].Quantity += quantity
			hasProductInCart = true
			break
		}
	}

	if hasProductInCart {
		err = rs.saveCartToRedis(shoppingCart, userID)
		if err != nil {
			logger.ZSLogger.Errorw("error updating user shopping cart", "err", err, "user", userID)
			return cart, err
		}
		return shoppingCart, err
	}

	shoppingCart.Products = append(shoppingCart.Products, models.ShoppingCartProducts{
		ProductID: productID,
		Quantity:  quantity,
	})

	err = rs.saveCartToRedis(shoppingCart, userID)
	if err != nil {
		logger.ZSLogger.Errorw("error updating user shopping cart", "err", err, "user", userID)
		return cart, err
	}

	return shoppingCart, err
}

// RemoveProductFromUserCart removes a product from user's shopping cart. It decrements the number of products the user have by the provided quantity.
// If the resulting quantity equals to zero, the product will be removed from the cart.
func (rs *RedisearchService) RemoveProductFromUserCart(userID string, productID string, quantity int) (cart models.ShoppingCart, err error) {
	// check if the product exists
	product, err := rs.SearchProductByID(productID)
	if product.ID == "" {
		return cart, constants.ErrNoProductsFound
	}
	shoppingCart, err := rs.GetUserCart(userID)
	if err != nil {
		// if the user has no existing cart, we create a new cart
		if err == redis.Nil {
			return cart, constants.ErrUserDoesNotHaveCart
		}
		logger.ZSLogger.Errorw("error getting user cart from redis", "err", err, "user", userID)
		return cart, constants.ErrInternal
	}

	var hasProductInCart bool
	var productToBeRemovedIndex int
	for i := 0; i < len(shoppingCart.Products); i++ {
		if shoppingCart.Products[i].ProductID == productID {
			if shoppingCart.Products[i].Quantity < quantity {
				return cart, constants.ErrNotEnoughProductsInCart
			} else if shoppingCart.Products[i].Quantity == quantity {
				productToBeRemovedIndex = i
			} else {
				shoppingCart.Products[i].Quantity -= quantity
			}
			hasProductInCart = true
			break
		}
	}

	if hasProductInCart {
		shoppingCart.Products = helpers.RemoveElementFromProductsSlice(shoppingCart.Products, productToBeRemovedIndex)
		err = rs.saveCartToRedis(shoppingCart, userID)
		if err != nil {
			logger.ZSLogger.Errorw("error updating user shopping cart", "err", err, "user", userID)
			return cart, err
		}
		return shoppingCart, err
	} else {
		return cart, constants.ErrUserDoesNotHaveTheProductInCart
	}
}

// GetUserCart fetches user's cart from redis and parses it
func (rs *RedisearchService) GetUserCart(userID string) (cart models.ShoppingCart, err error) {
	key := fmt.Sprintf("%s%s", constants.CartsRedisKey, userID)
	result, err := rs.redisDriver.Conn().Get(key).Result()
	if err != nil {
		logger.ZSLogger.Errorw("error reading carts key from redis", "err", err, "key", key)
		return cart, constants.ErrInternal
	}
	var shoppingCart models.ShoppingCart
	err = json.Unmarshal([]byte(result), &shoppingCart)
	if err != nil {
		logger.ZSLogger.Errorw("error unmarshalling redis result into shopping cart struct", "err", err)
		return cart, constants.ErrInternal
	}
	return shoppingCart, err
}

// createProductsSchema this function creates the products' schema in redisearch and fill it with sample products
func (rs *RedisearchService) createProductsSchema() (err error) {
	productsSchema := redisearch.NewSchema(redisearch.DefaultOptions).
		AddField(redisearch.NewTextFieldOptions("id", redisearch.TextFieldOptions{Sortable: true})).
		AddField(redisearch.NewTextFieldOptions("name", redisearch.TextFieldOptions{Sortable: true})).
		AddField(redisearch.NewNumericFieldOptions("price", redisearch.NumericFieldOptions{Sortable: true}))
	err = rs.productsClient.Drop()
	if err != nil && err.Error() != constants.ErrUnknownIndex {
		logger.ZSLogger.Errorw("error dropping index", "err", err)
		return constants.ErrInternal
	}
	err = rs.productsClient.CreateIndex(productsSchema)
	if err != nil {
		logger.ZSLogger.Errorw("error creating products index", "err", err)
		return constants.ErrInternal
	}

	err = mocks.GenerateMockProducts(rs.productsClient)
	if err != nil {
		logger.ZSLogger.Errorw("error generating mock products", "err", err)
		return err
	}
	return err
}

// saveCartToRedis saves the given shopping cart model in redis
func (rs *RedisearchService) saveCartToRedis(cart models.ShoppingCart, userID string) (err error) {
	key := fmt.Sprintf("%s%s", constants.CartsRedisKey, userID)
	b, err := json.Marshal(cart)
	if err != nil {
		logger.ZSLogger.Errorw("error marshalling shopping cart", "err", err)
		return constants.ErrInternal
	}
	err = rs.redisDriver.Conn().Set(key, string(b), 0).Err()
	if err != nil {
		logger.ZSLogger.Errorw("error setting carts key in redis", "err", err, "key", key)
		return constants.ErrInternal
	}

	return err
}
