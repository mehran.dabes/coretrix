package services

import (
	"coretrix/constants"
	"coretrix/helpers/logger"
	"time"

	"github.com/go-redis/redis"
)

//RedisInterface redis interface
type RedisInterface interface {
	Connect() error
	Conn() redis.UniversalClient
	healthPing()
}

//Driver struct
type Driver struct {
	RedisInterface
	DSN                 []string
	InternalPoolTimeout int64
	IdleTimeout         int64
	ReadTimeout         int64
	WriteTimeout        int64
	PingTry             bool

	conn redis.UniversalClient
}

//Connect new redis
func (r *Driver) Connect() error {

	// create redis universal client
	r.conn = redis.NewUniversalClient(&redis.UniversalOptions{
		Addrs:        r.DSN,
		PoolTimeout:  time.Duration(r.InternalPoolTimeout) * time.Second,
		IdleTimeout:  time.Duration(r.IdleTimeout) * time.Second,
		ReadTimeout:  time.Duration(r.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(r.WriteTimeout) * time.Second,
	})

	// first ping
	_, err := r.conn.Ping().Result()
	if err != nil {
		logger.ZSLogger.Errorw("error connecting to redis", "err", err)
		return constants.ErrInternal
	}

	// health ping
	if r.PingTry {
		r.healthPing()
	}

	return nil
}

//Conn get connection
func (r Driver) Conn() redis.UniversalClient {
	return r.conn
}

//healthPing start health ping
func (r Driver) healthPing() {
	ticker := time.NewTicker(2 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				_, err := r.conn.Ping().Result()
				if err != nil {
					<-quit
					_ = r.Connect()
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
