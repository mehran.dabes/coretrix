package mocks

import (
	"coretrix/constants"
	"coretrix/helpers/logger"
	"github.com/RediSearch/redisearch-go/redisearch"
)

// GenerateMockProducts Generates a number of sample products and saves them in the products index in redisearch
func GenerateMockProducts(redisearchClient *redisearch.Client) (err error) {
	data := []struct {
		ID    string
		Name  string
		Price float64
	}{
		{
			ID:    "1",
			Name:  "Product 1",
			Price: 100.0,
		},
		{
			ID:    "2",
			Name:  "Product 2",
			Price: 200.0,
		},
		{
			ID:    "3",
			Name:  "Product 3",
			Price: 300.0,
		},
		{
			ID:    "4",
			Name:  "Product 4",
			Price: 400.0,
		},
		{
			ID:    "5",
			Name:  "Product 5",
			Price: 500.0,
		},
		{
			ID:    "6",
			Name:  "Product 6",
			Price: 600.0,
		},
		{
			ID:    "7",
			Name:  "Product 7",
			Price: 700.0,
		},
		{
			ID:    "8",
			Name:  "Product 8",
			Price: 800.0,
		},
		{
			ID:    "9",
			Name:  "Product 9",
			Price: 900.0,
		},
		{
			ID:    "10",
			Name:  "Product 10",
			Price: 1000.0,
		},
	}

	for _, d := range data {
		doc := redisearch.NewDocument(d.ID, 1.0)
		doc.Set("name", d.Name).
			Set("price", d.Price)
		err = redisearchClient.Index([]redisearch.Document{doc}...)
		if err != nil {
			logger.ZSLogger.Errorw("error indexing new document", "doc", doc)
			return constants.ErrInternal
		}
	}
	return err
}
