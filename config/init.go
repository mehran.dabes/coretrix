package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	Port           string
	RedisearchHost string
	RedisearchPort string
}

var Configs Config

func Init() (err error) {
	viper.SetConfigFile("config/configs.yaml")
	err = viper.ReadInConfig()
	if err != nil {
		return err
	}

	Configs = Config{
		Port:           viper.GetString("port"),
		RedisearchHost: viper.GetString("redisearch_host"),
		RedisearchPort: viper.GetString("redisearch_port"),
	}
	return err
}
