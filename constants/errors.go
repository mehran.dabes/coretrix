package constants

import (
	"errors"
)

var (
	ErrUnknownIndex                    = "Unknown Index name"
	ErrInternal                        = errors.New("something went wrong")
	ErrUserDoesNotHaveTheProductInCart = errors.New("this user does not have any of this product in their shopping cart")
	ErrNoProductsFound                 = errors.New("there are no products with the provided id")
	ErrUserDoesNotHaveCart             = errors.New("this user has no shopping carts currently")
	ErrNotEnoughProductsInCart         = errors.New("cannot remove more than the user has in the shopping cart")
)
